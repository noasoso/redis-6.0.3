# redis-aware
1 .  Under the LFU policy, the access heat of the key is accumulated. 
    If it exceeds a certain threshold, 
    it is pushed to the corresponding client through publish / subscribe, 
    and the access heat is reduced   
   > 在LFU策略下， 累计key的访问热度。 如果超过某阈值，就通过发布/订阅 推送到对应的client，并降低访问热度
   
   
   
2 .  The delpush command is added to delete the key and push it to the corresponding 
     client in an asynchronous manner.
     In this way, we can ensure the consistency of redis and local cache.
  >  增加了delpush 命令，用异步的方式，删除key，并且推送到对应的client。这样，我们就能保证redis和本地缓存的一致性。


# 补充
本人gitee地址[https://gitee.com/TangBoHo](https://gitee.com/TangBoHo "Editor.md"),
对订单 秒杀 QA redis略有学习，欢迎交流 微信号 18210601309




